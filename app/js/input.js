const Key = Object.freeze({
    Space: 32,
    Left: 37,
    Up: 38,
    Right: 39,
    Down: 40,
    A: 65,
    D: 68,
    S: 83,
    W: 87,
    Mouse: 300,
    B: 66,
    C: 67
});

class Input {
    constructor() {
        this.keys = {};
        this.keysOld = {};

        window.addEventListener('focus', e => this.onWindowFocus(e) , false);
        window.addEventListener('keydown', e => this.onKeyDown(e), false);
        window.addEventListener('keyup', e => this.onKeyUp(e), false);
        window.addEventListener('mousedown', e => this.onMouseDown(e), false);
        window.addEventListener('mouseup', e => this.onMouseUp(e), false);
    }

    onMouseDown(event) {
        this.keys[300] = true;
    }

    onMouseUp(event) {
        delete this.keys[300];
    }

    onWindowFocus() {
        this.keys = {};
        this.keysOld = {};
    }

    onKeyDown(event) {
        this.keys[event.keyCode] = true;
    }

    onKeyUp(event) {
        delete this.keys[event.keyCode];
    }

    keyDown(key) {
        return this.keys[key] !== undefined;
    }

    keyPressed(key) {
        return this.keysOld[key] === undefined && this.keyDown(key);
    }

    keyReleased(key) {
        return this.keysOld[key] && !this.keyDown(key);
    }

    flush() {
        this.keysOld = Object.assign({}, this.keys);
    }
}

const input = new Input();

module.exports = {
    input,
    Key
};