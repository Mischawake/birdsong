---
- hosts: bootstrap
  user: root
  gather_facts: no
  vars:
    auth_dir: auth
    data_dir: data
    new_user: mrio

  pre_tasks:
    - name: Install python2
      raw: sudo apt-get -y install python-simplejson

  tasks:
  - name: Add admin group
    group:
      name=admin
      system=no
    become: yes

  - name: Give paswordless sudo to admin
    lineinfile:
      dest=/etc/sudoers
      state=present
      regexp='^%admin ALL\='
      line='%admin ALL=(ALL:ALL) NOPASSWD:ALL'
      validate='visudo -cf %s'
    become: yes

  - name: Create user {{ new_user }}
    user:
      name={{ new_user }}
      groups=admin
      createhome=yes
      shell=/bin/bash
    become: yes

  - name: Create ssh dir
    file:
      path=/home/{{ new_user }}/.ssh
      state=directory
      owner={{ new_user }}
      group={{ new_user }}
      mode=0755
    become: yes

  - name: Copy ssh key
    copy:
      src={{ auth_dir }}/machine.pub
      dest=/home/{{ new_user }}/.ssh/authorized_keys
      owner={{ new_user }}
      group={{ new_user }}
      mode=0644
    become: yes

  - name: Copy sshd config
    copy:
      src={{ data_dir }}/sshd_config
      dest=/etc/ssh/sshd_config
      owner=0
      group=0
      mode=0644
    become: yes

  - name: Restart sshd
    service:
      name=sshd
      state=restarted

  - name: Update ssh connection
    set_fact:
      ansible_ssh_user: "{{ new_user }}"
      ansible_ssh_port: 5022
      ansible_ssh_private_key_file: auth/machine

  - name: Configure ufw defaults
    ufw:
      direction={{ item.direction }}
      policy={{ item.policy }}
    with_items:
      - { direction: 'incoming', policy: 'deny' }
      - { direction: 'outgoing', policy: 'allow' }
    become: yes

  - name: Configure ufw rules
    ufw:
      rule={{ item.rule }}
      port={{ item.port }}
      proto={{ item.proto }}
    with_items:
      - { rule: 'allow', port: '80', proto: 'tcp' }
      - { rule: 'allow', port: '5022', proto: 'tcp' }
    become: yes

  - name: Enable ufw
    ufw:
      state=enabled
    become: yes

  - name: Install fail2ban
    apt:
      pkg=fail2ban
      state=present
      update_cache=yes
    become: yes

  - name: Configure fail2ban
    copy:
      src={{ data_dir }}/fail2ban.conf
      dest=/etc/fail2ban/jail.local
    become: yes

  - name: Restart fail2ban
    shell: fail2ban-client reload
    become: yes

  - name: Add nodejs repo
    shell:
      curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
    args:
      warn: no

  - name: Install packages
    apt:
      name={{ item }}
      state=present
      update_cache=yes
    with_items:
      - htop
      - emacs-nox
      - build-essential
      - nodejs
    become: yes
