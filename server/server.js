require('console-stamp')(console, {pattern: 'yyyy-mm-dd HH:MM:ss.L'});
const express = require('express');
const http = require('http');
const url = require('url');
const WebSocket = require('ws');
const Game = require('./game.js');

//
// Server
//
let app = express();
let dir = __dirname + '/../app';
console.log('Serving static files from', dir);
app.use(express.static(dir));

let server = http.createServer(app);
let wss = new WebSocket.Server({ server });

let gameManager = new Game.GameManager();

wss.on('connection', function(ws, req) {
    let loc = url.parse(req.url, true).path;

    ws.name = req.connection.remoteAddress + ':' + req.connection.remotePort;
    console.log('Connected %s to %s', ws.name, loc);

    if (loc == '/play') {
        gameManager.onConnection(ws);
    }
});

const kPort = process.env.NODE_ENV == 'production' ? 80 : 8080;
server.listen(kPort, function() {
    console.log('Server listening on port %d', server.address().port);
});
