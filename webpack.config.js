let webpack = require('webpack');
let path = require('path');
let CopyWebpackPlugin = require('copy-webpack-plugin');

const config = {
    devtool: 'eval',
    context: path.resolve(__dirname, 'app'),
    entry: './app.js',
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname, 'dist/app')
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.js$/,
                exclude: /(node_modules|deps|data)/,
                loader: 'eslint-loader',
                options: {
                    cache: true
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: ['env'],
                    cacheDirectory: true
                }
            }
        ]
    },
    plugins: [
        new CopyWebpackPlugin([
            {from: '.'}
        ], {
            ignore: ['*.js']
        })
    ],
    devServer: {
        compress: true,
        host: '0.0.0.0',
        disableHostCheck: true
    }
};

module.exports = config;
